package impl

import (
	"gitlab.com/go-course-project/go13/vblog/apps/token"
	"gitlab.com/go-course-project/go13/vblog/apps/user"
	"gitlab.com/go-course-project/go13/vblog/conf"
	"gitlab.com/go-course-project/go13/vblog/ioc"
	"gorm.io/gorm"
)

var (
	_ token.Service = (*TokenServiceImpl)(nil)
)

func init() {
	// new(TokenServiceImpl)
	ioc.Controller().Registry(token.AppName, &TokenServiceImpl{})
}

func NewTokenServiceImpl(userServiceImpl user.Service) *TokenServiceImpl {
	return &TokenServiceImpl{
		// 获取全局的DB对象
		// 前提: 配置对象准备完成
		db:   conf.C().DB(),
		user: userServiceImpl,
	}
}

// 怎么实现token.Service接口?
// 定义TokenServiceImpl来实现接口
type TokenServiceImpl struct {
	// 依赖了一个数据库操作的链接池对象
	db *gorm.DB

	// 依赖user.Service, 没有 UserServiceImpl 具体实现
	// 依赖接口，不要接口的具体实现
	user user.Service
}

// 对象属性初始化
func (i *TokenServiceImpl) Init() error {
	i.db = conf.C().DB()
	i.user = ioc.Controller().Get(user.AppName).(user.Service)
	return nil
}

func (i *TokenServiceImpl) Destory() error {
	return nil
}

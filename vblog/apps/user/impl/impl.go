package impl

import (
	"gitlab.com/go-course-project/go13/vblog/apps/user"
	"gitlab.com/go-course-project/go13/vblog/conf"
	"gitlab.com/go-course-project/go13/vblog/ioc"
	"gorm.io/gorm"
)

// 通过Import 自动完成注册
// 为什么不能直接在这里把db对象给初始化了?
// 这个逻辑是在import时候执行的, 程序在import 后才，执行的配置模块加载, 此时的conf.C()为nil
// 这个db属性的初始化一定要在配置加载后执行: conf.Load(), ioc.Init()
func init() {
	ioc.Controller().Registry(user.AppName, &UserServiceImpl{})
}

func NewUserServiceImpl() *UserServiceImpl {
	return &UserServiceImpl{
		// 获取全局的DB对象
		// 前提: 配置对象准备完成
		db: conf.C().DB(),
	}
}

// 怎么实现user.Service接口?
// 定义UserServiceImpl来实现接口
type UserServiceImpl struct {
	// 依赖了一个数据库操作的链接池对象
	db *gorm.DB
}

func (i *UserServiceImpl) Init() error {
	i.db = conf.C().DB()
	return nil
}

func (i *UserServiceImpl) Destory() error {
	// delete map
	return nil
}

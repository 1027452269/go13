// profile.js
var firstName = 'Michael';
var lastName = 'Jackson';
var year = 1948;


// 通过把变量组装成一个map 导出
// {firstName: firstName, lastName:lastName, year: year}
// export {firstName, lastName, year}; 


// 唯一的全局变量MYAPP:
// export var MYAPP = {
//     firstName, 
//     lastName, 
//     year}


// deault =  {firstName: firstName, lastName:lastName, year: year}
export default {
    firstName, 
    lastName, 
    year
}

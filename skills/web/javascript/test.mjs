// {firstName: firstName, lastName:lastName, year: year}
// map解构赋值
// { firstName, lastName, year } = {firstName: firstName, lastName:lastName, year: year}
// import { MYAPP } from './profile.mjs';

// 能不能通过包名来应用包里面的变量
// pkg.firstName
// pkg.lastName
// pkg.year

// var firstName = 'xxx'

// 可以通过别名的方式，对导入的变量进行重命名
// console.log(firstName, MYAPP.firstName, MYAPP.lastName, MYAPP.year)

// 还是需要知道有MYAPP, pkg.xxx
// { firstName, lastName, year } = {firstName: firstName, lastName:lastName, year: year}
// default as pkg
import profile from './profile.mjs'
console.log(profile.firstName, profile.lastName, profile.year)



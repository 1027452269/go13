
// Js 变量作用域
{
    var a = 100
    {
        console.log(a)
    }
    
    console.log(a)
}

// 解构赋值
// a1  = [1,2,3]
// x = a1[0]
// y = a1[1]
// z = a1[2]
// 变量数组 <-map-> value数组
[x, [y, z]] = [1, [2, 3]]
console.log(x,y,z)

// 对象的解构赋值
b1 = {name: '张三', age: 10}
var {name, age} = b1
// name = b1[name]
// age = b1[age]
console.log(name, age)

// go fmt fmt.Printf("你好 ! %s, 我是%s", "张三", "李四")
// console.log('你好 !' + name + ', 我是李四')
console.log(`你好 ! ${name}, 我是李四`)

// 错误处理
var  a = 'test'
// 中间逻辑, 不小心把a 修改为null或者undefine了
// 通过try catch 捕获代码片段异常
// try {
//     a = null
//     console.log(a[0])
// } catch (error) {
//     console.log(error)
// } finally {
//     console.log("finally logic")
// }


// 收到抛出业务异常
try {
    // 1. new exception
    // Error 是一种类型
    var e = new Error('这定义异常')
    // 2. 再抛出 exception
    throw e
} catch (error) {
    console.log(error)
}

// 函数
function abs(x) {
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
}
console.log(abs(-10))


// 方法: 绑定在对象上面的函数, 
var person = {name: '张三', age: 10}
person.greet = function() {
    // this 指代当前绑定的对象
    console.log(`你好! 我是${this.name}`)
}
// 这里的greet就是方法
person.greet()


// 等价于下面这个函数
// function sq(x) {
//     return x * x;
// }
//  x => x *x
// 定义函数的参数和返回值, 并没有定义函数名称
var sq = (x) => {return x *x } 
console.log(sq(10))

// for of 遍历array
arr1 = [1, 2, 3]
for (var x of arr1) {
    // fn(x)
    console.log(x);
}
// for of 遍历map
var obj = {
    name: 'Jack',
    age: 20,
    city: 'Beijing'
};
// Object.keys, Array/Object/Error
// 工具方法 
for (var key of Object.keys(obj)) {
    console.log(obj[key]);
}


// 使用 array对象提供一个跌倒器, 传递一个函数作为参数
// fn = (element => {   }
// 他不能中断循环
arr1.forEach(element => {
    console.log(element)
});